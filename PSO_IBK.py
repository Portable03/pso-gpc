#Import Dependencies

from __future__ import division
from random import random
from random import uniform
import numpy as np

#MAIN


class Particle:
    def __init__(self, x_0):
        self.vel_i = []
	self.pos_i = []         
        self.pos_best_i = []
	self.err_i = -1
	self.err_best_i = -1          
                  
                    
        for i in range(0, 1):
            self.vel_i.append(uniform(-1, 1))
            self.pos_i.append(x_0[i])

    # evaluation of current fitness
    def CFit(self, costFunc):
        self.err_i = costFunc(np.array(self.pos_i), eval_gradient=False)
        # self.err_i = costFunc(np.array(self.pos_i))

        print("###### error ########")
        print(self.err_i)
        print("##############")

        # confirm if current position is an individual best
        if self.err_i < self.err_best_i or self.err_best_i == -1:
            self.pos_best_i = self.pos_i.copy()
            self.err_best_i = self.err_i

    #new particle velocity update
    def vel_update(self, pos_best_g):
        w = 0.5		# weigh previous velocity
        c_1 = 1       	# cognative constant
        c_2 = 2       	# social constant

        for i in range(0, 1):
            r_1 = random()
            r_2 = random()

            vel_cog = c_1*r_1*(self.pos_best_i[i]-self.pos_i[i])
            vel_soc = c_2*r_2*(pos_best_g[i]-self.pos_i[i])
            self.vel_i[i] = w*self.vel_i[i]+vel_cog+vel_soc

    # particle position update considering updated velocity
    def pos_update(self, bounds):
        for i in range(0, 1):
            self.pos_i[i] = self.pos_i[i]+self.vel_i[i]

            # possible maximum position adjustment
            if self.pos_i[i] > bounds[i][1]:
                self.pos_i[i] = bounds[i][1]

            # possible minimum position adjustment
            if self.pos_i[i] < bounds[i][0]:
                self.pos_i[i] = bounds[i][0]


class PSO:
    def optimize(self, costFunc, x_0, bounds, num_particles, maxiter, verbose=False):

        global num_dimensions

        num_dimensions = len(x_0)
        pos_best_g = []                   # best position for group
	err_best_g = -1                   # best error for group
        
        #SWARM
        swarm = []
        for i in range(0, num_particles):
            swarm.append(Particle(x_0))

        # optimization loop
        i = 0
        while i < maxiter:
            if verbose:
                print("iteration: {}, best solution: {}".format(i, err_best_g))

            # evaluate fitness while cycling through particles in swarm 
            for j in range(0, num_particles):
                swarm[j].CFit(costFunc)

                # check if current particle is the global best
                if swarm[j].err_i < err_best_g or err_best_g == -1:
                    pos_best_g = list(swarm[j].pos_i)
                    err_best_g = float(swarm[j].err_i)

            # update velocities and position during swarm cycle
            for j in range(0, num_particles):
                swarm[j].vel_update(pos_best_g)
                swarm[j].pos_update(bounds)
            i += 1

        return np.array(pos_best_g), err_best_g

#END